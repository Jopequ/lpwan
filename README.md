# LPWAN

This repository contains reference documents for different LPWAN Technologies, mainly LoRa.

## Folder Structure

Content is divided in 4 folders, with information about (1) LoRa modulation and LoRaWAN; (2) Symphony Link; (3) Ingenu RPMA; and (4) Sigfox.

### LoRa and LoRaWAN

Contains documents about **LoRa** modulation basics (SEMTECH), the **LoRaWAN** specification and

**Research on MAC over LoRa**

[0] [MoT: A Deterministic Latency MAC Protocol for Mission-Critical IoT Applications](https://ieeexplore.ieee.org/abstract/document/8450355/)

## Recommended Papers

[1] [Low Power Wide Area Networks: An Overview](https://ieeexplore.ieee.org/document/7815384)

[2] [A Comparative Survey of LPWA Networking](https://arxiv.org/abs/1802.04222)

[3] [A Study of LoRa Long Range Low Power Networks for the Internet of Things](https://www.mdpi.com/1424-8220/16/9/1466)

[4] [Understanding the Limits of LoRaWAN](https://ieeexplore.ieee.org/document/8030482/)

